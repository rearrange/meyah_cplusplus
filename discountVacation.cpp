#include<iostream>
#include<string>
#include<iomanip>
#include<cstdlib>

using namespace std;

// Declare constant - original price before discount
// Note - kalau tak belajar lagi pasal const, boleh buang "const" tu, declare macam biasa je.
// Variable global sebab letak before main().
const double oriFlightLangkawi = 120.00;
const double oriFlightPangkor  = 90.00;
const double oriFlightTioman   = 150.00;
const double oriHotelLangkawi  = 260.00;
const double oriHotelPangkor   = 330.00;
const double oriHotelTioman    = 390.00;

int main()
{
    int inputDestination, inputTotalAdult, inputTotalChildren;
    double totalPrice, totalHotelPrice, totalFlightPrice;
    double discountHotelPrice = 0;
    double discountFlightPrice = 0;

    cout << "Enter 1/2/3 for your destination (1 - Langkawi, 2 - Pangkor, 3 - Tioman): ";
    cin >> inputDestination;

    cout << "How many adults will be going? : ";
    cin >> inputTotalAdult;

    cout << "How many children will tag along? : ";
    cin >> inputTotalChildren;

    // Aku malas main dgn char/tolower, so inputDestination = int.
    if (inputDestination == 1){ //Destination = Langkawi

        // Calculate discount for Hotel - Adult
        if (inputTotalAdult > 2)
            discountHotelPrice = (oriHotelLangkawi * 0.3) * inputTotalAdult;
        else if (inputTotalAdult == 2)
            discountHotelPrice = (oriHotelLangkawi * 0.2) * inputTotalAdult;
        else
            discountHotelPrice = 0;

        // Calculate discount for Hotel - Children
        // Need to add the discountHotelPrice calculated for adult.
        if (inputTotalChildren > 0) {
            // Reuse discountHotelPrice variable, need to add earlier calculation above!
            discountHotelPrice = discountHotelPrice + ((oriHotelLangkawi * 0.5) * inputTotalChildren);

            //Calculate discount for Flight - Children
            discountFlightPrice = (oriFlightLangkawi * 0.5) * inputTotalChildren;
        }

        // Calculate total prices
        totalHotelPrice = (oriHotelLangkawi * (inputTotalAdult + inputTotalChildren)) - discountHotelPrice;
        totalFlightPrice = (oriFlightLangkawi * (inputTotalAdult + inputTotalChildren)) - discountFlightPrice;
        totalPrice = totalHotelPrice + totalFlightPrice;

        // Print calculation
        cout << "Original Hotel Price: RM " << oriHotelLangkawi << endl;
        cout << "Original Flight Price: RM " << oriFlightLangkawi << endl;
        cout << "Hotel discount: RM " << discountHotelPrice << endl;
        cout << "Flight discount: RM " << discountFlightPrice << endl;
        cout << "Hotel price after discount : RM " << totalHotelPrice << endl;
        cout << "Flight price after discount : RM " << totalFlightPrice << endl;
        cout << "Total need to pay: RM " << totalPrice << endl;
    }

    else if (inputDestination == 2) { // Destination = Pangkor

      // Calculate discount for Hotel - Adult
      if (inputTotalAdult > 2)
          discountHotelPrice = (oriHotelPangkor * 0.3) * inputTotalAdult;
      else if (inputTotalAdult == 2)
          discountHotelPrice = (oriHotelPangkor * 0.2) * inputTotalAdult;
      else
          discountHotelPrice = 0;

      // Calculate discount for Hotel - Children
      // Need to add the discountHotelPrice calculated for adult.
      if (inputTotalChildren > 0) {
          // Reuse discountHotelPrice variable, need to add earlier calculation above!
          discountHotelPrice = discountHotelPrice + ((oriHotelPangkor * 0.5) * inputTotalChildren);

          //Calculate discount for Flight - Children
          discountFlightPrice = (oriFlightPangkor * 0.5) * inputTotalChildren;
      }

      // Calculate total prices
      totalHotelPrice = (oriHotelPangkor * (inputTotalAdult + inputTotalChildren)) - discountHotelPrice;
      totalFlightPrice = (oriFlightPangkor * (inputTotalAdult + inputTotalChildren)) - discountFlightPrice;
      totalPrice = totalHotelPrice + totalFlightPrice;

      // Print calculation
      cout << "Original Hotel Price: RM " << oriHotelPangkor << endl;
      cout << "Original Flight Price: RM " << oriFlightPangkor << endl;
      cout << "Hotel discount: RM " << discountHotelPrice << endl;
      cout << "Flight discount: RM " << discountFlightPrice << endl;
      cout << "Hotel price after discount : RM " << totalHotelPrice << endl;
      cout << "Flight price after discount : RM " << totalFlightPrice << endl;
      cout << "Total need to pay: RM " << totalPrice << endl;
    }

    else if (inputDestination == 3) { // Destination = Tioman

      // Calculate discount for Hotel - Adult
      if (inputTotalAdult > 2)
          discountHotelPrice = (oriHotelTioman * 0.3) * inputTotalAdult;
      else if (inputTotalAdult == 2)
          discountHotelPrice = (oriHotelTioman * 0.2) * inputTotalAdult;
      else
          discountHotelPrice = 0;

      // Calculate discount for Hotel - Children
      // Need to add the discountHotelPrice calculated for adult.
      if (inputTotalChildren > 0) {
          // Reuse discountHotelPrice variable, need to add earlier calculation above!
          discountHotelPrice = discountHotelPrice + ((oriHotelTioman * 0.5) * inputTotalChildren);

          //Calculate discount for Flight - Children
          discountFlightPrice = (oriFlightTioman * 0.5) * inputTotalChildren;
      }

      // Calculate total prices
      totalHotelPrice = (oriHotelTioman * (inputTotalAdult + inputTotalChildren)) - discountHotelPrice;
      totalFlightPrice = (oriFlightTioman * (inputTotalAdult + inputTotalChildren)) - discountFlightPrice;
      totalPrice = totalHotelPrice + totalFlightPrice;

      // Print calculation
      cout << "Original Hotel Price: RM " << oriHotelTioman << endl;
      cout << "Original Flight Price: RM " << oriFlightTioman << endl;
      cout << "Hotel discount: RM " << discountHotelPrice << endl;
      cout << "Flight discount: RM " << discountFlightPrice << endl;
      cout << "Hotel price after discount : RM " << totalHotelPrice << endl;
      cout << "Flight price after discount : RM " << totalFlightPrice << endl;
      cout << "Total need to pay: RM " << totalPrice << endl;

    }

    cout << "Calculation complete, exiting ... ";
    return 0;
}
