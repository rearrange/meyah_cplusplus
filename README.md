# README #

### What is this repository for? ###

* Collection of codes answering my sister's assignment.

### How do I get set up? ###

* Compile the code using C++ compiler. 
    * g++ -o <output_name> <source_code.cpp>
    * E.g: g++ -o 12421stairs.out 12421stairs.cpp
* Run the compiled output. 
    * ./output_name
    * E.g: ./12421stairs.out

