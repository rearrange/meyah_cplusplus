#include<iostream>

using namespace std;

int main()
{
    int row, col, val;

    //row counter
    for(row = 1; row < 7; row++) {

        //print initial 1
        val = 1;
        cout << val << ' ';

        //column counter
        for(col = 1; col < ((row * 2) - 1); col++) {

            //print value for column
            if(col < row) {
                val *= 2;
                cout << val << ' ';
            }

            else {
                val /=2;
                cout << val << ' ';
            }
        }

        //print next line; ending row
        cout << endl;
    }

    return 0;
}
